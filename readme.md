# Compilation Technique (COMP 6062)

## Programming Language Translator / Final Assignment

### Description
The purpose of this assignment is to test student's capability of analysing language using parser algorithm. The program will receive input and will try to analyse the input and then converted into another language.

### Technology Used
As the assignment specification did not said about the programming language, I use `Python 3` to create the program. Below are used technologies and libraries for this project:
>* [Python 3](python.org)
* [竜 TatSu](github.com/neogeny/TatSu)
* [Processing](processing.org)

### Installing the dependencies
The source code uses a library called `竜 TatSu`, a library that uses "LR" parsing method. Before running the python file, you need to install `竜 TatSu` by simply execute this command:

>`pip3 install tatsu`

### How to run
Simply execute the command `python main.py` and it will bring up the instructions and how to use it. Here are some screenshot of the example:

![Using my program](img/source.png)

After you have execute `compile` command, it will everything that is on the stack (Sentences that you have inserted successfully). Here are the compiled code:

![Compiled Processing code](img/translated.png)

If you run the program, it will display the image that you have created:

![Result](img/result.png)

### Easter Egg
Try to execute `hidden` function inside my program and see what the instructions told so!
