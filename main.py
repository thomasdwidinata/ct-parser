import pprint;
import json;
import re;
import os;
from copy import deepcopy;
from tatsu import parse;
from tatsu.util import asjson;

processing_template = '''
void setup()
{
  size(800,600);
  background(255);

  // below are the translated code

''';

CFG = '''
START → draw OBJ on POSITION
OBJ → BOX | TRIANGLE | CIRCLE | LINE
BOX → box ( DWI )
TRIANGLE → triangle ( DWI )
CIRCLE → circle ( NUMBER )
LINE → line( CATUR )
EKA → (NUMBER)
DWI → (NUMBER , NUMBER)
TRI → (NUMBER , NUMBER, NUMBER)
CATUR → (NUMBER, NUMBER, NUMBER, NUMBER)
POSITION → ( DWI )
NUMBER → any possible integer number
''';

available_features = {
    'box':'rect($,$,$,$);',
    'triangle':'triangle($,$,$,$,$,$);',
    'circle':'ellipse($,$,$,$);',
    'line':'line($,$,$,$);',
    'indonesian child drawing':'size(800,600);background(255);line(0,450,800,450);triangle(20,450,225,300,450,450);triangle(350,450,575,275,800,450);ellipse(400,200,150,150);line(400, 110, 400, 60);line(470, 140, 510, 100);line(495, 200, 555, 200);line(470, 260, 510, 300);line(400, 295, 400, 350);line(330, 260, 295, 300);line(300, 200, 245, 200);line(330, 140, 290, 100);line(0,515, 300, 515);line(300,515,265, 700);line(500, 515, 800, 515);line(500, 515, 600, 700);line(400, 525, 400, 540);line(400, 555, 400, 570);line(400, 585, 400, 600);'
};

g = '''
    @@grammar::ELEMENTARY

    START = 'draw' OBJ 'on' POSITION;
    OBJ = BOX | TRIANGLE | CIRCLE | LINE | INDO;
    BOX = 'box' DWI;
    TRIANGLE = 'triangle' DWI;
    CIRCLE = 'circle' EKA;
    LINE = 'line' CATUR;
    INDO = 'indonesian child drawing';

    EKA = '(' NUMBER ')';
    DWI = '(' NUMBER ',' NUMBER ')';
    TRI = '(' NUMBER ',' NUMBER ',' NUMBER')';
    CATUR = '(' NUMBER ',' NUMBER ',' NUMBER ',' NUMBER ')';
    POSITION = 'position' DWI;

    NUMBER = /\d+/ ;
''';

array_list = [];
list_of_commands = []

curpath = os.path.abspath(os.curdir);
default_path = os.path.join(curpath, 'compiled.pde');

def triangle(height, length, coordinate_x, coordinate_y):
    return [coordinate_x, coordinate_y, int(coordinate_x) + int(length), coordinate_y, (((int(coordinate_x) + int(length)) - int(coordinate_x)) / 2) + int(coordinate_x), int(coordinate_y) + int(height)];

def box(h, w, c_x, c_y):
    return [c_x, c_y, h, w];

def circle(d, c_x, c_y):
    return [c_x, c_y, d, d];

def line(one,two,three,four, c_x, c_y):
    return [int(one) + int(c_x),int(two) + int(c_y),three,four];

def writeFile(datas):
    global default_path;
    f = open(default_path, 'w+');
    f.write(datas);
    f.close();

def generate(data):
    global array_list;
    result = '';
    temp = '';
    a = 0;
    for available in list(available_features):
        if(data[1][0] == available):
            temp = available_features[available];
            temp = list(temp);
            quick_mafs = [];
            if(available == 'box'):
                quick_mafs = box(data[1][1][1], data[1][1][3], data[3][1][1], data[3][1][3]);
            elif(available == 'triangle'):
                quick_mafs = triangle(data[1][1][1], data[1][1][3], data[3][1][1], data[3][1][3]);
            elif(available == 'circle'):
                quick_mafs = circle(data[1][1][1], data[3][1][1], data[3][1][3]);
            elif(available == 'line'):
                quick_mafs = line(data[1][1][1], data[1][1][3], data[1][1][5], data[1][1][7],data[3][1][1], data[3][1][3]);
            for i in range(0, len(temp)):
                if(temp[i] == '$'):
                    temp[i] = str(quick_mafs[a]);
                    a += 1;
        elif(data[1] == 'indonesian child drawing'):
            temp = available_features[available];
            temp = list(temp);
        result = ''.join(temp);
    #print(result);
    array_list.append(result);

def main():
    global array_list;
    print("Elementary Drawing!\nMade by:\nAndre Valentino\nThomas Dwi Dinata\n\nSyntax Sample:\n'Draw box(5,10) on position (100,100)'\n'Draw triangle(5,5) on position(25,25)'");
    print('''
Commands:
    compile - Compile all commands from Stack
    stack - List all stack
    clear - Clear the Stack
    hidden - Show the hidden feature
    exit - Exit the program
Grammar:
    draw OBJECT on CARTESIAN_COORDINATE
Where:
    OBJECT :
        Box(width,height)
        Triangle(length, height)
        Circle(Diametre)
        Line(First X Coordinate, First Y Coordinate, Last X Coordinate, Last Y Coordinate)
    CARTESIAN_COORDINATE :
        Position(X Coordinate, Y Coordinate)
    ''');
    print("Ready.");
    while True:
        try:
            ori = input('>> ');

            # Huge Switch Alternative
            if(ori == 'exit'):
                print("Exiting...");
                exit(0);
            elif(ori == 'compile'):
                template = deepcopy(processing_template);
                for p_code in array_list:
                    template += '\t' + p_code;
                template += "\n}";
                writeFile(template);
                print("Compiled and file saved into : " + default_path);
                array_list = [];
                continue;
            elif(ori == ''):
                print("Enter a command after '>>' !\n");
                continue;
            elif(ori == 'hidden'):
                print("You found a hidden feature! Type\n\n\tdraw indonesian child drawing on position(0,0)\n\nto see the hidden feature!\n");
                continue;
            elif(ori == 'clear'):
                print("Stack cleared!");
                continue;
            elif(ori == 'stack'):
                print("Here is the list of job you have submitted:");
                print(list_of_commands);
                print('');
                continue;

            # If nothing is wrong, continue~
            s = ori.lower();
        except EOFError:
            break;
        try:
            竜 = parse(g, s);
            generate(竜);
            list_of_commands.append(s);
            print("OK: ",end="");
            print(竜);
        except Exception as e:
            print(e);

def tester():
    try:
        while(True):
            ori = input('>> ');
            s = ori.lower();
            竜 = parse(g, s);
            print('OK : ', end='');
            print(竜);
    except Exception as e:
        print(e);


def test():
    try:
        print(parse(g, "draw box(5) on position(400,200)"));
        print(parse(g, "draw a box(5) on position(500,500)"));
    except Exception as e:
        print(e);
    exit(0);

if __name__ == '__main__':
    #test();
    #tester();
    main();
